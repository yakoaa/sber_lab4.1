package dao;

import java.util.ArrayList;

public interface EntityDAO {

    <T> T findOneById(Integer id);

    ArrayList findAll();
}
