package dao;

import entity.Flat;
import org.hibernate.Session;
import org.hibernate.Transaction;
import utill.HibernateConnectionFactory;

import java.util.ArrayList;

public class FlatDAO implements EntityDAO {
    public Flat findOneById(Integer id) {
        Flat flat;

        Session session = HibernateConnectionFactory.getSessionFactory().openSession();
        flat = session.get(Flat.class, id);
        session.close();

        return flat;
    }

    public ArrayList<Flat> findAll() {
        ArrayList<Flat> flats;

        Session session = HibernateConnectionFactory.getSessionFactory().openSession();
        flats = (ArrayList<Flat>) session.createQuery("From Flat").list();
        session.close();

        return flats;
    }

    public void save(Flat flat) {
        Session session = HibernateConnectionFactory.getSessionFactory().openSession();
        Transaction tr = session.beginTransaction();

        session.save(flat);

        tr.commit();
        session.close();
    }

    public void delete(Flat flat) {
        Session session = HibernateConnectionFactory.getSessionFactory().openSession();
        Transaction tr = session.beginTransaction();

        session.delete(flat);

        tr.commit();
        session.close();
    }

    public void update(Flat flat) {
        Session session = HibernateConnectionFactory.getSessionFactory().openSession();
        Transaction tr = session.beginTransaction();

        session.update(flat);

        tr.commit();
        session.close();
    }
}
