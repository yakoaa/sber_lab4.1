package dao;

import entity.Human;
import org.hibernate.Session;
import org.hibernate.Transaction;
import utill.HibernateConnectionFactory;

import java.util.ArrayList;

public class HumanDAO implements EntityDAO {

    public Human findOneById(Integer id) {
        Human human;
        Session session = HibernateConnectionFactory.getSessionFactory().openSession();

        human = session.get(Human.class, id);

        session.close();

        return human;
    }


    public ArrayList<Human> findAll() {
        ArrayList<Human> humans;
        Session session = HibernateConnectionFactory.getSessionFactory().openSession();

        humans = (ArrayList<Human>) session.createQuery("From Human").list();

        session.close();

        return humans;
    }


    public void delete(Human human) {
        Session session = HibernateConnectionFactory.getSessionFactory().openSession();
        Transaction tr = session.beginTransaction();

        session.delete(human);
        tr.commit();

        session.close();
    }

    public void update(Human human) {
        Session session = HibernateConnectionFactory.getSessionFactory().openSession();
        Transaction tr = session.beginTransaction();

        session.update(human);
        tr.commit();

        session.close();
    }

    public void save(Human human) {
        Session session = HibernateConnectionFactory.getSessionFactory().openSession();
        Transaction tr = session.beginTransaction();

        session.save(human);

        tr.commit();
        session.close();
    }
}
