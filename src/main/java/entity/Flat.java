package entity;

import javax.persistence.*;

@Entity
public class Flat {
    @Id
    @GeneratedValue
    private Integer id;

    @Column
    private Integer number;

    @ManyToOne
    private Human human;

    public Flat(Integer id, Integer number, Human human) {
        this.id = id;
        this.number = number;
        this.human = human;
    }

    public Flat() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public Human getHuman() {
        return human;
    }

    public void setHuman(Human human) {
        this.human = human;
    }

    @Override
    public String toString() {
        return "Flat{" +
                "id=" + id +
                ", number=" + number +
                ", human=" + human +
                '}';
    }
}
