package service;

import dao.FlatDAO;
import entity.Flat;

import java.util.ArrayList;

public class FlatService {
    FlatDAO flatDAO = new FlatDAO();


    public Flat findOne(Integer id){
        return flatDAO.findOneById(id);
    }

    public ArrayList<Flat> findAll(){
        return flatDAO.findAll();
    }

    public void delete(Flat flat){
        flatDAO.delete(flat);
    }

    public void save(Flat flat){
        flatDAO.save(flat);
    }

    public void update(Flat flat){
        flatDAO.update(flat);
    }
}
