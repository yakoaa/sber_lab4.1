package service;

import dao.HumanDAO;
import entity.Human;

import java.util.ArrayList;

public class HumanService {

    HumanDAO humanDAO = new HumanDAO();

    public Human findOne(Integer id){
        return humanDAO.findOneById(id);
    }

    public ArrayList<Human> findAll(){
        return humanDAO.findAll();
    }

    public void delete(Human human){
        humanDAO.delete(human);
    }

    public void save(Human human){
        humanDAO.save(human);
    }

    public void update(Human human){
        humanDAO.update(human);
    }
}
