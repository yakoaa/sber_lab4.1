package service;

import entity.Flat;
import entity.Human;

public class SomeActionBean {

    FlatService flatService = new FlatService();
    HumanService humanService = new HumanService();

    public void addHuman(String name, Integer age){

        Human human = new Human();

        human.setAge(age);
        human.setName(name);

        humanService.save(human);
    }

    public Human getHumans(Integer id){
        Human human;
        human = humanService.findOne(id);
        return human;
    }

    public void addFlat(Integer num, Human human){

        Flat flat = new Flat();

        flat.setNumber(num);
        flat.setHuman(human);

        flatService.save(flat);
    }
}
