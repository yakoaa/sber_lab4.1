package utill;

import entity.Flat;
import entity.Human;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

public class HibernateConnectionFactory {
    private static SessionFactory sessionFactory;

    public HibernateConnectionFactory() {
    }
    public static SessionFactory getSessionFactory()
    {
        if (sessionFactory == null)
            try
            {
                Configuration configuration = new Configuration().configure();
                configuration.addAnnotatedClass(Human.class);
                configuration.addAnnotatedClass(Flat.class);

                StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties());
                sessionFactory = configuration.buildSessionFactory(builder.build());
            }
            catch(Exception e)
            {
                throw new RuntimeException("Исключение при получении сессии! " + e);
            }
        return sessionFactory;
    }
}
